import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatButtonModule, MatDialogModule, MatListModule, MatProgressBarModule } from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { DialogComponent } from './dialog/dialog.component'
import { UploadComponent } from './upload.component'

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,
    FlexLayoutModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
  ],
  declarations: [UploadComponent, DialogComponent],
  exports: [UploadComponent],
  entryComponents: [DialogComponent]
})
export class UploadModule {}