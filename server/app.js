const express = require('express');
const cors = require('cors');
const upload = require('./upload');
const submission = require('./submission')

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
};

const app = express();
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/submission/:code', submission.getSubmission);
app.post('/step', submission.completeStep)
app.post('/upload', upload);

app.listen(8001, '0.0.0.0', () => console.log('Server started!'));