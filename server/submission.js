const mysql = require('mysql');

exports.getSubmission = function (req, res) {

  let code = req.params.code;
  // First you need to create a connection to the db
  const con = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'user',
    password: 'user',
    database: 'rc2019_onboarding'
  });

  con.connect((err) => {
    if (err) {
      console.log('Error connecting to Db');
      return;
    }
    console.log('Connection established');
  });


  let escapedCode = code; // submission code received from the user on STEP 1
  con.query('SELECT abstract_title, firstname, lastname FROM submissioncodes WHERE unique_identifier = ?', [escapedCode],  // [TASK 2]: write the sql query to fetch the record based the `unique_identifier` field
    (err, rows) => {
      if (err) {
        res.send(500);
        throw err;
      }

      console.log('Data received from Db:\n');
      console.log(rows);

      if(rows.length > 1) {
        return res.status(500).send('got more than one records');
      }

      return res.status(200).send(rows[0]);
    });


  con.end((err) => {
    // The connection is terminated gracefully
    // Ensures all previously enqueued queries are still
    // before sending a COM_QUIT packet to the MySQL server.
  });

}



  // [TASK 3]: implement the completeStep function
  // - extract the code (`unique_identifier`) & step nubmer from req.body
  // - connect to DB
  // - perform SQL query to update the corresponding field based on the step number of the right record 
  // e.g. for step=2 you should update `step_2` field
  // - respond to the HTTP Post request if the above action finish without errors 
exports.completeStep = function(req, res) {

  let code = req.body.code;
  let nubmer = req.body.step;

  // First you need to create a connection to the db
  const con = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'user',
    password: 'user',
    database: 'rc2019_onboarding'
  });

  con.connect((err) => {
    if (err) {
      console.log('Error connecting to Db');
      return;
    }
    console.log('Connection established');
  });

  let m = {1:'step_1', 2:'step_2',3:'step_3',4:'step_4'};
  let escapedCode = code; // submission code received from the user on STEP 1
  let mystep = nubmer;
  con.query('UPDATE submissioncodes SET ?? = 1 WHERE unique_identifier = ?', [m[mystep],escapedCode],  (err, rows) => {
    if (err) {
      res.send(500);
      throw err;
    }

    console.log('Step updated from Db:\n');
    console.log(rows);

    return res.status(200).send(rows[0]);
  });


  console.log(req.body);
  return res.status(200).send();
  con.end((err) => {
    // The connection is terminated gracefully
    // Ensures all previously enqueued queries are still
    // before sending a COM_QUIT packet to the MySQL server.
  });
}

